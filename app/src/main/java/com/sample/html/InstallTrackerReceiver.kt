package com.sample.html

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

import com.google.android.gms.analytics.CampaignTrackingReceiver

class InstallTrackerReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        CampaignTrackingReceiver().onReceive(context, intent)
    }
}
