package com.sample.html

import android.content.Context
import android.content.SharedPreferences

class PreferenceData {

    val PREFS_SETTINGS = "MyAppPreff"
    val KEY_REFERRING_LINK = "referringLink"
    val KEY_THANK_YOU_URL = "thankYouUrl"
    val KEY_PACKAGE_ID = "packageId"
    val KEY_DEVICE_ID = "deviceId"
    val KEY_IS_PREMIUM = "isPremium"
    val KEY_IS_DEEPLINK_USER = "isDeeplinkUser"
    val KEY_OPENEXT = "openExt"
    val KEY_ADVERTISING_ID = "AdvertisingId"
    val KEY_DEEPLINK_OBJECT = "deeplinkobj"
    val KEY_ADD_TO_INSTALLS = "addtoinstall"
    val KEY_ID_INSTALL = "idinstall"
    val KEY_ENDP = "endp"
    val KEY_LOAD_URL = "loadUrl"

    fun setStringPrefs(prefKey: String, context: Context,
                       Value: String) {
        val settings = context.getSharedPreferences(
                PREFS_SETTINGS, 0)
        val editor = settings.edit()
        editor.putString(prefKey, Value)
        editor.commit()
    }

    fun getStringPrefs(prefKey: String, context: Context): String? {
        val settings = context.getSharedPreferences(
                PREFS_SETTINGS, 0)
        return settings.getString(prefKey, "")
    }

    fun setBooleanPrefs(prefKey: String, context: Context,
                        value: Boolean?) {
        val settings = context.getSharedPreferences(
                PREFS_SETTINGS, 0)
        val editor = settings.edit()
        editor.putBoolean(prefKey, value!!)
        editor.commit()
    }

    fun getBooleanPrefs(prefKey: String, context: Context): Boolean {
        val settings = context.getSharedPreferences(
                PREFS_SETTINGS, 0)
        return settings.getBoolean(prefKey, false)
    }

}
