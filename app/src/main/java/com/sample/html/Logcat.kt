package com.sample.html

import android.util.Log


class Logcat {

    /*
     * ALLOW_LOG to enable or disable debugging log
     */
    var ALLOW_LOG = true


    /*
     * @since API level 1
     *
     * Send an DEBUG log message.
     */
    fun e(tag: String, message: String) {
        if (ALLOW_LOG) {
            Log.e(tag, message)
        }
    }

    /*
     * @since API level 1
	 *
	 * Send an DEBUG log message.
	 *
	 */

    fun d(tag: String, message: String) {
        if (ALLOW_LOG) {
            Log.d(tag, message)
        }
    }

    /*
     * @since API level 1
     *
     * Send an VERBOSE log message.
     */
    fun v(tag: String, message: String) {
        if (ALLOW_LOG) {
            Log.v(tag, message)
        }
    }


    /*
     * @since API level 1
     *
     * Send an INFO log message.
     */
    fun i(tag: String, message: String) {
        if (ALLOW_LOG) {
            Log.i(tag, message)
        }
    }

    /*
     * @since API level 1
     *
     * Send a WARN log message.
     */
    fun w(tag: String, message: String) {
        if (ALLOW_LOG) {
            Log.w(tag, message)
        }
    }


    /*
     * @since API level 8
     *
     * What a Terrible Failure: Report a condition that should never happen.
     * The error will always be logged at level ASSERT with the call stack.
     * Depending on system configuration, a report may be added to the DropBoxManager
     * and/or the process may be terminated immediately with an error dialog.
     */
    fun wtf(tag: String, message: String) {
        if (ALLOW_LOG) {
            Log.wtf(tag, message)
        }
    }
}