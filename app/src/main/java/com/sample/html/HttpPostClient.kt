package com.sample.html

import org.json.JSONObject

import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader

import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.RequestBody

class HttpPostClient {
    private val TAG = "HttpPostClient"
    fun sendHttpPost(URLs: String, jsonObjSend: JSONObject): JSONObject? {


        var jsonObjectResp: JSONObject? = null

        try {
            val JSON = MediaType.parse("application/json; charset=utf-8")
            val client = OkHttpClient()

            val body = RequestBody.create(JSON, jsonObjSend.toString())

            val request = okhttp3.Request.Builder()
                    .url(URLs)
                    .post(body)
                    .build()
            val response = client.newCall(request).execute()
            val networkResp = response.body()!!.string()

            if (!networkResp.isEmpty()) {
                jsonObjectResp = parseJSONStringToJSONObject(networkResp)

            }
        } catch (ex: Exception) {
            val err = String.format("{\"result\":\"false\",\"error\":\"%s\"}", ex.message)
            Logcat().e("httppost", "httppost ex :$ex")
            jsonObjectResp = parseJSONStringToJSONObject(err)
        }

        return jsonObjectResp
    }

    private fun parseJSONStringToJSONObject(strr: String): JSONObject? {
        Logcat().e("result ", "parseJSONStringToJSONObject :$strr")
        var response: JSONObject? = null
        try {
            response = JSONObject(strr)
        } catch (ex: Exception) {
            try {
                response = JSONObject()
                response.put("result", "failed")
                response.put("data", strr)
                response.put("error", ex.message)
            } catch (exx: Exception) {
            }

        }

        return response
    }

}