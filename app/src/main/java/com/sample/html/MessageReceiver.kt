package com.sample.html

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import com.google.android.gms.auth.api.phone.SmsRetriever
import com.google.android.gms.common.api.CommonStatusCodes
import com.google.android.gms.common.api.Status

class MessageReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        val action = intent.action
        try {
            if (SmsRetriever.SMS_RETRIEVED_ACTION == action) {
                val extras = intent.extras
                val status = extras!!.get(SmsRetriever.EXTRA_STATUS) as Status
                Log.e(TAG, "onReceive: $status")
                when (status.statusCode) {
                    CommonStatusCodes.SUCCESS -> {
                        Log.e(TAG, "onReceive: SUCCESS")
                        val smsMessage = extras.get(SmsRetriever.EXTRA_SMS_MESSAGE) as String
                        Log.e(TAG, "Retrieved sms code: $smsMessage")
                        Logcat().e("MessageReceiver", "Retrieved sms code: $smsMessage")
                        if (smsMessage != null && smsMessage.length > 0) {
                            val activityHTML = ActivityHTML.instance
                            if (activityHTML != null) {
                                activityHTML!!.fillSMS(smsMessage)
                            }
                        }
                    }
                    CommonStatusCodes.TIMEOUT -> Log.e(TAG, "onReceive: TIMEOUT")
                    else -> {
                    }
                }
            }
        } catch (var7: Exception) {
            Logcat().e("MessageReceiver", "Exception : $var7")
        }

    }

    companion object {
        private val TAG = "MessageReceiver"
    }
}

