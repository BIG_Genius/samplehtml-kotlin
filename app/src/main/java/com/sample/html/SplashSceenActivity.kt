package com.sample.html

import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.TextView

import org.json.JSONObject

import java.net.URLDecoder
import java.util.Locale

import io.branch.referral.Branch
import io.branch.referral.BranchError


class SplashSceenActivity : AppCompatActivity() {
    private var mTextViewVersion: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        try {
            setContentView(R.layout.activity_splashscreen)
        } catch (e: Exception) {
            Logcat().e(tag, "home error $e")
        }

        try {
            supportActionBar!!.hide()
        } catch (e: Exception) {

        }

        try {
            AppSignatureHelper(this@SplashSceenActivity).getAppSignatures(this@SplashSceenActivity)[0]
        } catch (e: Exception) {
            Log.e(tag, "Exception: $e")
        }

    }

    private fun navigateToApp() {

        if (Utility().getAdvertisingID(this@SplashSceenActivity) != null && Utility().getAdvertisingID(this@SplashSceenActivity) != "") {
            sendToTheHome()
        } else {
            object : AsyncTask<Context, Void, String>() {
                override fun doInBackground(vararg params: Context): String {

                    val innerContext = params[0]
                    val innerResult = Utility().generateAdvertisingId(innerContext)
                    Logcat().e("info", "GoogleAdId read $innerResult")
                    Utility().setAdvertisingID(innerContext, innerResult)
                    return innerResult
                }

                override fun onPostExecute(playAdiId: String) {
                    Logcat().e("gpsadid", "id->$playAdiId")
                    Utility().setAdvertisingID(this@SplashSceenActivity, playAdiId)
                    sendToTheHome()
                }
            }.execute(this@SplashSceenActivity)
        }
    }

    private fun sendToTheHome() {
        if (Utility().isDeeplinkUser(this@SplashSceenActivity)) {
            val intent = Intent(this@SplashSceenActivity, ActivityHTML::class.java)
            startActivity(intent)
        } else {
            val intent = Intent(this@SplashSceenActivity, MainActivity::class.java)
            startActivity(intent)
        }
        finish()
    }

    fun setupVersionDetails() {
        try {
            mTextViewVersion = findViewById(R.id.textview_version)
            mTextViewVersion!!.text = "Version: " + BuildConfig.VERSION_NAME
        } catch (e: Exception) {
        }

    }

    override fun onStart() {
        super.onStart()
        if (intent.flags and Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT != 0) {
            finish()
            return
        }
        setupVersionDetails()
        Branch.getInstance().initSession({ referringParams, error ->
            if (error == null) {
                Logcat().e("BRANCH SDK", referringParams.toString())
                val sessionParams = Branch.getInstance().latestReferringParams
                Logcat().e("sessions params :", sessionParams.toString())
                processDeepLinkParams(sessionParams)
            } else {

                Logcat().e("BRANCH SDK", error.message)
            }
            navigateToApp()
        }, this.intent.data, this)
    }

    public override fun onNewIntent(intent: Intent) {
        this.intent = intent
    }

    fun processDeepLinkParams(deeplinkObj: JSONObject?) {
        try {
            if (!Utility().isPremiumUser(this@SplashSceenActivity)) {

                if (deeplinkObj!!.has("source") && deeplinkObj.get("source").toString() == "ma" || deeplinkObj.has("OpenPage") && deeplinkObj.get("OpenPage").toString() == "BScreenActivity") {

                    try {
                        Utility().getReferringLinkFromDeeplink(deeplinkObj, this@SplashSceenActivity)
                    } catch (e: Exception) {

                    }

                    try {
                        Utility().setDeeplinkObject(this@SplashSceenActivity, deeplinkObj.toString().replace("\\", ""))
                    } catch (e: Exception) {

                    }

                    Utility().setLogs(deeplinkObj)
                    try {
                        if (deeplinkObj != null && deeplinkObj.has("endp") && !deeplinkObj.isNull("endp")) {
                            Utility().saveEndp(this@SplashSceenActivity, deeplinkObj.get("endp").toString())

                            if (Utility().getAdvertisingID(this@SplashSceenActivity) != null && Utility().getAdvertisingID(this@SplashSceenActivity) !== "") {
                                CallInstallApi(this@SplashSceenActivity).execute()
                            } else {
                                object : AsyncTask<Context, Void, String>() {
                                    override fun doInBackground(vararg params: Context): String {

                                        val innerContext = params[0]
                                        val innerResult = Utility().generateAdvertisingId(innerContext)
                                        Logcat().e("info", "GoogleAdId read $innerResult")
                                        Utility().setAdvertisingID(innerContext, innerResult)
                                        return innerResult
                                    }

                                    override fun onPostExecute(playAdiId: String) {
                                        Logcat().e("gpsadid", "id->$playAdiId")
                                        Utility().setAdvertisingID(this@SplashSceenActivity, playAdiId)
                                        CallInstallApi(this@SplashSceenActivity).execute()
                                    }
                                }.execute(this@SplashSceenActivity)
                            }
                        }

                    } catch (e: Exception) {

                    }

                    try {
                        if (deeplinkObj != null && deeplinkObj.has("loadurl") && !deeplinkObj.isNull("loadurl")) {
                            Utility().setLoadURL(this@SplashSceenActivity, "https://" + deeplinkObj.get("loadurl").toString())
                        }
                    } catch (e: Exception) {

                    }

                    if (deeplinkObj.has("OpenPage") && deeplinkObj.get("OpenPage").toString() == "BScreenActivity") {
                        Utility().setDeeplinkUser(this@SplashSceenActivity, true)
                    }
                }
            }


        } catch (e: Exception) {
            Logcat().e("json problem", e.toString())
        }

    }

    class CallInstallApi internal constructor(internal var context: Context) : AsyncTask<String, Int, JSONObject>() {
        override fun onPreExecute() {
            super.onPreExecute()
        }

        override fun doInBackground(vararg params: String): JSONObject? {

            val mainJson = JSONObject()
            var url = ""
            try {
                url = Utility().getEndp(context)!! + "/API/InAppWAP/InstallCallBack/Default.aspx?flowName=BScreenActivity"

                mainJson.put("model", Utility().model)
                mainJson.put("deviceId", Utility().getDeviceIdFromDevice(context))
                mainJson.put("gpsAdid", Utility().getAdvertisingID(context))
                mainJson.put("platformId", 1)
                mainJson.put("os", "android")
                mainJson.put("osVersion", Utility().osVersion)
                mainJson.put("packageName", Utility().getPackageName(context))
                mainJson.put("appVersionCode", Utility().appVersionCode)
                mainJson.put("referringLink", URLDecoder.decode(Utility().getReferringLink(context), "UTF-8"))
                mainJson.put("deepLinkObj", Utility().getDeeplinkObject(context))
                mainJson.put("typeOfBuild", Utility().typeOfBuild)
                mainJson.put("lang", Locale.getDefault().language)
                Logcat().e(tag, "url :$url")
                Logcat().e(tag, "installJson: $mainJson")


            } catch (e: Exception) {
                e.printStackTrace()

            }

            return HttpPostClient().sendHttpPost(url, mainJson)
        }

        override fun onPostExecute(result: JSONObject?) {
            try {
                Logcat().e(tag, "install result: " + result!!)

                if (result != null) {
                    readJsonObjectForInstall(result, context)

                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

            super.onPostExecute(result)
        }
    }

    companion object {
        val tag = "SplashScreenActivity"
        fun readJsonObjectForInstall(jsonObject: JSONObject?, context: Context) {
            try {
                Logcat().e(tag, "jsonObject: " + jsonObject!!.toString())
                if (jsonObject != null) {

                    if (jsonObject.has("IDInstall") && !jsonObject.isNull("IDInstall")) {
                        if (jsonObject.getInt("IDInstall") != -1) {
                            Utility().setIdInstall(context, jsonObject.get("IDInstall").toString())
                            Logcat().e(tag, "IdInstall :" + Utility().getIdInstall(context)!!)
                            Utility().setAddToInstall(context, true)
                        }
                    }

                }
            } catch (e: Exception) {
            }

        }
    }
}