package com.sample.html

import android.content.Context
import android.net.ConnectivityManager
import android.os.Build
import android.provider.Settings

import org.json.JSONObject

import java.net.URLEncoder
import java.nio.charset.Charset

class Utility {

    val appVersionCode: Int
        get() = BuildConfig.VERSION_CODE
    val typeOfBuild: Int
        get() = if (BuildConfig.DEBUG) {
            0
        } else 1


    val osVersion: String
        get() = Build.VERSION.RELEASE
    val model: String
        get() = Build.MODEL

    // Check internet connection
    fun isNetworkAvailable(context: Context): Boolean {
        val cm = context
                .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return if (cm == null || cm.activeNetworkInfo == null)
            false
        else
            cm
                    .activeNetworkInfo.isConnectedOrConnecting
    }

    fun setDeeplinkUser(context: Context, value: Boolean) {
        PreferenceData().setBooleanPrefs(PreferenceData().KEY_IS_DEEPLINK_USER, context, value)
    }

    fun isDeeplinkUser(context: Context): Boolean {
        //return true;
        return PreferenceData().getBooleanPrefs(PreferenceData().KEY_IS_DEEPLINK_USER, context)
    }


    fun setThankYouURL(context: Context, value: String) {
        PreferenceData().setStringPrefs(PreferenceData().KEY_THANK_YOU_URL, context, value)
    }

    fun getThankYouURL(context: Context): String? {
        return PreferenceData().getStringPrefs(PreferenceData().KEY_THANK_YOU_URL, context)
    }

    fun setReferringLink(context: Context, value: String) {
        PreferenceData().setStringPrefs(PreferenceData().KEY_REFERRING_LINK, context, value)
    }

    fun getReferringLink(context: Context): String? {
        return PreferenceData().getStringPrefs(PreferenceData().KEY_REFERRING_LINK, context)
    }

    fun setLoadURL(context: Context, value: String) {
        PreferenceData().setStringPrefs(PreferenceData().KEY_LOAD_URL, context, value)
    }

    fun getLoadURL(context: Context): String? {
        return PreferenceData().getStringPrefs(PreferenceData().KEY_LOAD_URL, context)
    }

    fun setPremiumUser(context: Context, value: Boolean) {
        PreferenceData().setBooleanPrefs(PreferenceData().KEY_IS_PREMIUM, context, value)
    }

    fun isPremiumUser(context: Context): Boolean {
        return PreferenceData().getBooleanPrefs(PreferenceData().KEY_IS_PREMIUM, context)
    }

    fun setDeviceID(context: Context, value: String) {
        PreferenceData().setStringPrefs(PreferenceData().KEY_DEVICE_ID, context, value)
    }

    fun getDeviceID(context: Context): String? {
        return PreferenceData().getStringPrefs(PreferenceData().KEY_DEVICE_ID, context)
    }

    fun setPackageID(context: Context, value: String?) {
        PreferenceData().setStringPrefs(PreferenceData().KEY_PACKAGE_ID, context, value!!)
    }

    fun getPackageID(context: Context?): String? {
        return PreferenceData().getStringPrefs(PreferenceData().KEY_PACKAGE_ID, context!!)
    }

    fun setOpenExt(context: Context, value: Boolean) {
        PreferenceData().setBooleanPrefs(PreferenceData().KEY_OPENEXT, context, value)
    }

    fun isOpenExt(context: Context): Boolean {
        return PreferenceData().getBooleanPrefs(PreferenceData().KEY_OPENEXT, context)
    }


    fun setAdvertisingID(context: Context, value: String) {
        PreferenceData().setStringPrefs(PreferenceData().KEY_ADVERTISING_ID, context, value)
    }

    fun getAdvertisingID(context: Context): String? {
        return PreferenceData().getStringPrefs(PreferenceData().KEY_ADVERTISING_ID, context)
    }

    fun saveEndp(context: Context, value: String) {
        PreferenceData().setStringPrefs(PreferenceData().KEY_ENDP, context, "https://$value")
    }

    fun getEndp(context: Context): String? {
        return PreferenceData().getStringPrefs(PreferenceData().KEY_ENDP, context)
    }

    fun getIdInstall(context: Context): String? {
        return PreferenceData().getStringPrefs(PreferenceData().KEY_ID_INSTALL, context)
    }

    fun setIdInstall(context: Context, value: String) {
        PreferenceData().setStringPrefs(PreferenceData().KEY_ID_INSTALL, context, value)
    }

    fun getAddToInstall(context: Context): Boolean {
        return PreferenceData().getBooleanPrefs(PreferenceData().KEY_ADD_TO_INSTALLS, context)
    }

    fun setAddToInstall(context: Context, value: Boolean?) {
        PreferenceData().setBooleanPrefs(PreferenceData().KEY_ADD_TO_INSTALLS, context, value)
    }


    fun setDeeplinkObject(context: Context, value: String) {
        PreferenceData().setStringPrefs(PreferenceData().KEY_DEEPLINK_OBJECT, context, value)
    }

    fun getDeeplinkObject(context: Context): String? {
        return PreferenceData().getStringPrefs(PreferenceData().KEY_DEEPLINK_OBJECT, context)
    }


    fun setLogs(deeplinkObj: JSONObject?) {
        try {

            if (deeplinkObj != null && deeplinkObj.has("logs") && deeplinkObj.get("logs").toString().equals("true", ignoreCase = true)) {
                Logcat().ALLOW_LOG = true
                return
            }

            Logcat().ALLOW_LOG = false

        } catch (e: Exception) {
            Logcat().ALLOW_LOG = false
        }

    }

    fun getReferringLinkFromDeeplink(deeplinkObj: JSONObject?, context: Context): String {

        try {
            if (deeplinkObj != null && deeplinkObj.has("~referring_link")) {
                val referringLink = deeplinkObj.getString("~referring_link").toString()
                if (referringLink != null && !referringLink.equals("", ignoreCase = true)) {
                    try {
                        setReferringLink(context, URLEncoder.encode(referringLink, "UTF-8"))
                    } catch (e: Exception) {

                    }

                    return referringLink
                }
            }
            return ""

        } catch (e: Exception) {
            Logcat().e("getReferringLinkFromDeeplink", "Exception: $e")
            return ""
        }

    }


    fun getPackageName(context: Context?): String? {

        var mPackageId: String? = ""
        mPackageId = Utility().getPackageID(context)
        return if (mPackageId != null && !mPackageId.isEmpty()) {
            mPackageId
        } else {
            try {
                if (context != null) {
                    mPackageId = context.packageName
                    Utility().setPackageID(context, mPackageId)
                    mPackageId
                } else {
                    ""
                }
            } catch (e: Exception) {
                Logcat().e("getPackageName error", "" + e.toString())
                ""
            }

        }
    }


    fun getDeviceIdFromDevice(context: Context): String {
        var android_id: String? = ""
        android_id = Utility().getDeviceID(context)
        if (android_id != null && !android_id.isEmpty()) {
            return android_id
        } else {
            android_id = Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
            if (android_id != null && !android_id.isEmpty()) {
                Utility().setDeviceID(context, android_id)
                return android_id
            }
            if (android_id == null) {
                android_id = ""
            }
            return android_id
        }
    }

    fun generateAdvertisingId(context: Context): String {
        var advertisingIdClient: AdvertisingIdClient.AdInfo? = null
        try {
            advertisingIdClient = AdvertisingIdClient().getAdvertisingIdInfo(context)
            return advertisingIdClient!!.id
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return ""
    }


}
