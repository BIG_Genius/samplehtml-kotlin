package com.sample.html

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.KeyEvent
import android.view.View
import android.webkit.CookieManager
import android.webkit.WebChromeClient
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient

import com.sample.html.R

class WebviewActivity : AppCompatActivity() {
    private var webView: WebView? = null
    private var link: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_webview)


        try {

            supportActionBar!!.hide()
        } catch (e: Exception) {

        }

        readExtra()
        initVGPlusWebView()

    }

    private fun readExtra() {
        if (intent.hasExtra("link") && intent.extras!!.getString("link") != null) {
            link = intent.extras!!.getString("link")
        }

    }

    fun initVGPlusWebView() {
        webView = findViewById<View>(R.id.webview_game) as WebView
        CookieManager.getInstance().setAcceptCookie(true)
        webView!!.settings.javaScriptEnabled = true
        webView!!.settings.useWideViewPort = true
        webView!!.settings.loadWithOverviewMode = true
        webView!!.settings.domStorageEnabled = true
        webView!!.settings.pluginState = WebSettings.PluginState.ON
        webView!!.webChromeClient = WebChromeClient()
        webView!!.visibility = View.VISIBLE

        webView!!.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView, url: String) {
                Logcat().e(tag, "onPageFinished url: $url")

                super.onPageFinished(view, url)
            }

            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {

                Logcat().e(tag, "shouldOverrideUrlLoading url: $url")


                return super.shouldOverrideUrlLoading(view, url)
            }
        }

        webView!!.loadUrl(link)
    }


    public override fun onResume() {
        super.onResume()
        webView!!.onResume()
    }

    public override fun onPause() {
        super.onPause()
        webView!!.onPause()
    }


    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (event.action == KeyEvent.ACTION_DOWN) {
            when (keyCode) {
                KeyEvent.KEYCODE_BACK -> {
                    if (webView!!.canGoBack()) {
                        webView!!.goBack()
                    } else {
                        finish()
                    }
                    return true
                }
            }

        }
        return super.onKeyDown(keyCode, event)
    }


    public override fun onDestroy() {
        super.onDestroy()
        webView!!.loadUrl("about:blank")
    }

    companion object {


        val tag = "WebviewActivity"
    }

}
