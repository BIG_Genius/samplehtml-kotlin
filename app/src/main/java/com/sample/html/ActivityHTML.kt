package com.sample.html

import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.provider.Browser
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.webkit.ValueCallback
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient

import com.facebook.appevents.AppEventsConstants
import com.facebook.appevents.AppEventsLogger
import com.google.android.gms.auth.api.phone.SmsRetriever
import com.google.android.gms.auth.api.phone.SmsRetrieverClient
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.gms.tasks.Task

import java.net.URLEncoder
import java.util.Locale

class ActivityHTML : AppCompatActivity(), VerifyPincode {

    private var webview: WebView? = null

    private var smsRetrieverClient: SmsRetrieverClient? = null
    private var messageReceiver: MessageReceiver? = null

    private var url = ""

    private var inputMethodManager: InputMethodManager? = null
    private var mContext: Context? = null
    private var checkNative = false

    companion object {
        lateinit var instance: ActivityHTML
    }

    init {
        instance = this
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_html)

        instance = this
        mContext = this
        webview = findViewById<View>(R.id.webview) as WebView
        webview!!.webViewClient = WebViewClient()
        webview!!.settings.javaScriptEnabled = true
        webview!!.settings.domStorageEnabled = true
        webview!!.overScrollMode = WebView.OVER_SCROLL_NEVER

        try {

            inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

            if (!Utility().isPremiumUser(this@ActivityHTML)) {
                messageReceiver = MessageReceiver()
                smsretrive()
                val filter = IntentFilter()
                filter.addAction(SmsRetriever.SMS_RETRIEVED_ACTION)
                registerReceiver(messageReceiver, filter)
            }

        } catch (e: Exception) {

        }

        url = "file:///android_asset/index.html?"
        if (!Utility().getLoadURL(this@ActivityHTML)!!.isEmpty() || !Utility().getLoadURL(this@ActivityHTML).equals("")) {
            url = Utility().getLoadURL(this@ActivityHTML) + "?"
        }

        url = url + "packageName=" + Utility().getPackageName(this@ActivityHTML)+
                "&osVersion=" + Utility().osVersion +
                "&appVersionCode=" + Utility().appVersionCode+
                "&typeOfBuild=" + Utility().typeOfBuild +
                "&platformId=1&model=" + Utility().model +
                "&lang=" + Locale.getDefault().language +
                "&deviceId=" + Utility().getDeviceIdFromDevice(this@ActivityHTML) +
                "&isPremium=" + Utility().isPremiumUser(this@ActivityHTML) +
                "&gpsAdid=" + Utility().getAdvertisingID(this@ActivityHTML) +
                "&referringLink=" + Utility().getReferringLink(this@ActivityHTML)

        if (Utility().isPremiumUser(this@ActivityHTML)) {
            try {
                url = url + "&thankYouUrl=" + URLEncoder.encode(Utility().getThankYouURL(this@ActivityHTML), "UTF-8")
            } catch (e: Exception) {

            }

        }
        webview!!.requestFocus(View.FOCUS_DOWN)
        webview!!.loadUrl(url)

        Logcat().e("HTMLActivity", "url: $url")

        webview!!.webChromeClient = WebChromeClient()

        webview!!.webViewClient = object : WebViewClient() {
            internal var result: String? = ""


            override fun onPageFinished(view: WebView, url: String) {
                Logcat().e("HTMLActivity", "onPageFinished url: $url")
                if (!Utility().isPremiumUser(this@ActivityHTML)) {
                    val handler = Handler()
                    handler.postDelayed({
                        if (!checkNative) {
                            val phoneFocus = "javascript:document.getElementById('phone');"
                            if (Build.VERSION.SDK_INT >= 19) {
                                webview!!.evaluateJavascript(phoneFocus) { s ->

                                    try {
                                        if (s != null && s != "null") {
                                            inputMethodManager!!.showSoftInput(webview, InputMethodManager.SHOW_IMPLICIT)
                                        }
                                        Logcat().e("HTMLActivity", "phoneFocus s: $s")
                                    } catch (e: Exception) {

                                    }


                                }
                            }
                        }
                    }, 5000)

                }


                super.onPageFinished(view, url)
            }

            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {

                Logcat().e("HTMLActivity", "shouldOverrideUrlLoading url: $url")
                val uri = Uri.parse(url)

                if (url != null && url.startsWith("sms:")) {
                    Logcat().e("HTMLActivity", "result sms")
                    webview!!.getContext().startActivity(Intent(Intent.ACTION_SENDTO, uri))
                    return true
                }

                try {
                    //save openExt
                    val openExt = uri.getQueryParameter("openExt")
                    if (openExt != null && openExt.length > 0) {
                        Logcat().e("HTMLActivity", "result openExt->$openExt")
                        if (openExt.toLowerCase().contains("true")) {
                            Utility().setOpenExt(this@ActivityHTML, true)
                        }

                    }

                    result = uri.getQueryParameter("result")
                    Logcat().e("HTMLActivity", "result res->" + result!!)
                } catch (e: Exception) {

                }

                if (result != null && result!!.equals("SUCCESS", ignoreCase = true) && url.contains("Thankyou.html")) {


                    try {

                        val reportToFb = uri.getQueryParameter("reportToFb")
                        if (reportToFb != null && reportToFb.length > 0 && reportToFb.toLowerCase().contains("true") && !Utility().isPremiumUser(this@ActivityHTML))
                            sendFBEvent()
                    } catch (e: Exception) {

                    }


                    Utility().setPremiumUser(this@ActivityHTML, true)
                    Logcat().e("HTMLActivity", "startCGView userStatus: premium")
                    Utility().setThankYouURL(this@ActivityHTML, url)

                    if (Utility().isOpenExt(this@ActivityHTML)) {
                        OpenPortalInExternalBrowser("$url&isPremium=true")
                    } else {
                        openPortalInAppBrowser("$url&isPremium=true")
                    }


                } else if (result != null && result!!.equals("Failed", ignoreCase = true) && url.contains("Thankyou.html")) {


                    if (Utility().isOpenExt(this@ActivityHTML)) {
                        OpenPortalInExternalBrowser("$url&isPremium=false")
                    } else {
                        openPortalInAppBrowser("$url&isPremium=false")
                    }


                } else if (url.contains("checknative.html")) {
                    checkNative = true
                    val intent = Intent(this@ActivityHTML, MainActivity::class.java)
                    intent.putExtra("checkNative", "Yes")
                    startActivity(intent)
                    finish()

                }
                return super.shouldOverrideUrlLoading(view, url)
            }
        }


    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (event.action == KeyEvent.ACTION_DOWN) {
            when (keyCode) {
                KeyEvent.KEYCODE_BACK -> {
                    if (webview!!.canGoBack()) {
                        webview!!.goBack()
                    } else {
                        finish()
                    }
                    return true
                }
            }

        }
        return super.onKeyDown(keyCode, event)
    }

    private fun openPortalInAppBrowser(url: String) {
        Logcat().e("HTMLActivity", "openPortalInAppBrowser: $url")
        val intent = Intent(this@ActivityHTML, WebviewActivity::class.java)
        intent.putExtra("link", url)
        startActivity(intent)
        finish()
    }

    private fun OpenPortalInExternalBrowser(url: String) {
        Logcat().e("HTMLActivity", "OpenPortalInExternalBrowser: $url")
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_MULTIPLE_TASK)
        intent.putExtra(Browser.EXTRA_APPLICATION_ID, mContext!!.packageName)
        try {
            intent.setPackage("com.android.chrome")
            startActivity(intent)
        } catch (e: Exception) {
            intent.setPackage(null)
            startActivity(intent)
        }

        finish()
    }

    override fun onPause() {
        try {

            if (currentFocus != null) {
                inputMethodManager!!.hideSoftInputFromWindow(currentFocus!!.applicationWindowToken, 0)
            }
        } catch (e: Exception) {

        }

        super.onPause()
    }

    override fun onDestroy() {

        try {

            if (currentFocus != null) {

                inputMethodManager!!.hideSoftInputFromWindow(currentFocus!!.applicationWindowToken, 0)
            }

        } catch (e: Exception) {

        }

        try {
            unregisterReceiver(messageReceiver)
        } catch (e: Exception) {

        }

        super.onDestroy()
    }

    fun smsretrive() {
        smsRetrieverClient = SmsRetriever.getClient(this@ActivityHTML)

        val task = smsRetrieverClient!!.startSmsRetriever()

        task.addOnSuccessListener { Log.e("HTMLActivity", "SmsRetrievalResult status: Success") }
        task.addOnFailureListener { e -> Log.e("HTMLActivity", "SmsRetrievalResult start failed.", e) }
    }

    override fun fillSMS(sms: String) {
        //https://android--examples.blogspot.com/2017/08/android-webview-fill-form-and-submit.html

        Log.e("HTMLActivity", "fillSMS sms$sms")
        val js = "javascript:document.getElementById('smstext').value = '$sms';"
        if (Build.VERSION.SDK_INT >= 19) {
            webview!!.evaluateJavascript(js) { s ->
                Log.e("HTMLActivity", "fillSMS onReceiveValue: $s")

                val click = "javascript:document.getElementById('parsepin').click();"

                if (Build.VERSION.SDK_INT >= 19) {
                    webview!!.evaluateJavascript(click) { s -> Log.e("HTMLActivity", "fillSMS click onReceiveValue: $s") }
                }
            }
        }

    }

    fun sendFBEvent() {
        val logger = AppEventsLogger.newLogger(this)
        logger.logEvent(AppEventsConstants.EVENT_NAME_SUBSCRIBE)
        logger.logEvent(AppEventsConstants.EVENT_NAME_COMPLETED_REGISTRATION)

    }


}
